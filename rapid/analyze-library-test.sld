;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid analyze-library-test)
  (import (scheme base)
	  (scheme file)
	  (scheme write)
	  (rapid receive)
	  (rapid vicinity)
          (rapid test)
	  (rapid comparator)
	  (rapid set)
	  (rapid mapping)
	  (rapid syntax)
	  (rapid analyze-library))
  (export run-tests)
  (begin
    (define (run-tests)
      
      (test-begin "Analyze R7RS libraries")

      (test-group "Comparators"

	(test-assert "Symbol comparator"
	  (comparator? symbol-comparator)))

      (test-group "Library names"

	(test-assert "library-name?: true"
	  (library-name? '(srfi 1)))

	(test-assert "library-name?: false"
	  (not (library-name? '(srfi (1)))))
	
	(test-assert "library-name-comparator"
	  (comparator? library-name-comparator))

	(test-equal "unwrap-library-name"
	  '(srfi 1)
	  (unwrap-library-name (syntax (srfi 1))))

	(test-error "unwrap-library-name: error"
	  (unwrap-library-name (syntax (srfi (1))))))
      
      
      (test-group "Qualified identifiers"
	(define qid (make-qualified-identifier #f '(srfi 1) 'foo))
	
	(test-assert "qualified-identifier?"
	  (qualified-identifier? qid))

	(test-equal "qualified-identifier-library-name"
	  '(srfi 1)
	  (qualified-identifier-library-name qid))

	(test-equal "qualified-identifier-symbol"
	  'foo
	  (qualified-identifier-symbol qid)))

      (test-group "Parameter objects"
	(parameterize
	    ((feature-identifier-predicate test-feature-identifier?)
	     (library-importability-predicate test-library-importable?)
	     (library-dependencies-accessor test-library-dependencies)
	     (library-exports-accessor test-library-exports))

	  (test-assert "feature-identifier?"
	    (feature-identifier? 'r7rs))

	  (test-assert "library-importable?"
	    (library-importable? #f '(scheme base)))

	  (test-assert "library-exports"
	    (mapping? (library-exports #f '(scheme base))))))

      (test-group "Library definitions"
	(parameterize
	    ((feature-identifier-predicate test-feature-identifier?)
	     (library-importability-predicate test-library-importable?)
	     (library-dependencies-accessor test-library-dependencies)
	     (library-exports-accessor test-library-exports))
	  
	  (define (qid->list qid)
	    (list (qualified-identifier-library-name qid)
		  (qualified-identifier-symbol qid)))
	  (define data
	    '((export rapid-cons)))
	  (define file-name (in-vicinity (user-vicinity) "data.scm"))
	  (define (write-file!)
	    (unless (file-exists? file-name)
	      (with-output-to-file file-name
		(lambda ()
		  (for-each write data)))))
	  (define data2
	    '((EXPORT rapid-cons)))
	  (define file-name2 (in-vicinity (user-vicinity) "data2.scm"))
	  (define (write-file2!)
	    (unless (file-exists? file-name2)
	      (with-output-to-file file-name2
		(lambda ()
		  (for-each write data2)))))
	  (define-syntax library
	    (syntax-rules ()
	      ((library declaration ...)
	       (receive result
		   (analyze-library '(rapid)
				    (list (syntax declaration) ...))
		 result))))

	  (define (imports library-definition . identifiers)
	    (let ((imports (list-ref library-definition 1)))
	      (map (lambda (identifier)
		     (qid->list (mapping-ref imports identifier)))
		   identifiers)))
	  
	  (write-file!)
	  (write-file2!)

	  (test-equal "expand-cond-expand"
	    '(1)
	    (map syntax->datum
		 (expand-cond-expand (list (syntax (r7rs 1))))))
	  
	  (test-equal "Library body"
	    '(foo bar)
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (begin foo
						      bar))))		
	      (map syntax->datum body)))
	  
	  (test-equal "Library include"
	    '((export rapid-cons))
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (include "data.scm"))))
	      (map syntax->datum body)))
	  
	  (test-equal "Library include-ci"
	    '((export rapid-cons))
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (include-ci "data2.scm"))))
	      (map syntax->datum body)))
	  
	  (test-equal "Library export: simple"
	    '(((scheme base) car) ((rapid) cons))
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (import (scheme base)))
				       (syntax (export car cons))))
	      (list (qid->list (mapping-ref exports 'car))
		    (qid->list (mapping-ref exports 'cons)))))

	  (test-equal "Library export: rename"
	    '(((scheme base) car) ((rapid) cons))
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (import (scheme base)))
				       (syntax (export car (rename cons
								   kons)))))
	      (list (qid->list (mapping-ref exports 'car))
		    (qid->list (mapping-ref exports 'kons)))))

	  (test-equal "include-library-declarations"
	    '(((rapid) rapid-cons))
	    (receive (exports imports dependencies body)
		(analyze-library
		 '(rapid)
		 (list (syntax (include-library-declarations "data.scm"))))
	      (list (qid->list (mapping-ref exports 'rapid-cons)))))

	  (test-equal "cond-expand"
	    '(2 5 6 7 9 11)
	    (receive (exports imports dependencies body)
		(analyze-library
		 '(rapid)
		 (unwrap-syntax
		  (syntax
		   ((cond-expand
		      ((library (scheme char))
		       (begin 1))
		      ((library (scheme base))
		       (begin 2))
		      (else
		       (begin 3)))
		    (cond-expand
		      (r6rs
		       (begin 4))
		      (r7rs
		       (begin 5)))
		    (cond-expand
		      (else
		       (begin 6)))
		    (cond-expand
		      ((not (library (scheme char)))
		       (begin 7)))
		    (cond-expand
		      ((and (library (scheme base))
			    (library (scheme char)))
		       (begin 8))
		      ((and (library (scheme base))
			    (library (srfi 1)))
		       (begin 9)))
		    (cond-expand
		      ((or (library (scheme char)))
		       (begin 10))
		      ((or (library (scheme char))
			   (library (srfi 1)))
		       (begin 11)))))))
	      (map syntax->datum body)))

	  (test-equal "import: simple"
	    '(((scheme base) car))
	    (imports (library (import (scheme base)))
		     'car))

	  (test-equal "import: rename"
	    '(((scheme base) car))
	    (imports (library (import (rename (scheme base) (car kar))))
		     'kar))

	  (test-equal "import: only"
	    '(((scheme base) car)
	      ((scheme base) car))
	    (imports (library (import (only (scheme base) car)
				      (rename (only (scheme base) car) (car cdr))))
		     'car 'cdr))

	  (test-equal "import: prefix"
	    '(((scheme base) car))
	    (imports (library (import (prefix (scheme base) prefix-)))
		     'prefix-car) )

	  (test-equal "import: except"
	    '(((scheme base) car)
	      ((scheme base) car))
	    (imports (library (import (except (scheme base) cdr)
				      (rename (only (scheme base) car) (car cdr))))
		     'car 'cdr))

	  (test-equal "Library dependencies"
	    '((scheme base))
	    (receive (exports imports dependencies body)
		(analyze-library '(rapid)
				 (list (syntax (import (scheme base)))))
	      (set->list dependencies)))
	  
	  (test-group "Library definition errors"

	    (test-error "identifier already in list"
              (library (import (only (scheme base) car car))))
	    
	    (test-error "identifier already imported with different binding"
	      (library (import (scheme base)
			       (rename (scheme base) (car cdr)))))

	    (test-error "only: identifier not found in original set"
	      (library (import (only (scheme base) case-lambda))))

	    (test-error "except: identifier not found in orignal set"
	      (library (import (except (scheme base) write-shared))))

	    (test-error "rename: identifier already in rename list"
	      (library (import (rename (scheme base) (car kar) (car cdr)))))
	    
	    (test-error "rename: identifier not found in original set"
	      (library (import (rename (scheme base) (write-shared write)))))

	    (test-error "rename: identifier already imported with a different binding"
	      (library (import (rename (scheme base) (car cdr))))))))
      
      (test-end))

    (define (test-feature-identifier? obj)
      (and (memq obj '(r7rs rapid-scheme)) #t))
    
    (define (test-library-importable? context library-name)
      (and (member library-name '((scheme base) (srfi 1))) #t))

    (define (test-library-dependencies library-name)
      (set library-name-comparator))
    
    (define (test-library-exports context library-name)
      (cond
       ((assoc library-name
	       `(((scheme base) .
		  ,(mapping symbol-comparator
			    'car (make-qualified-identifier #f '(scheme base) 'car)
			    'cdr (make-qualified-identifier #f '(scheme base) 'cdr)))))
	=> cdr)
       (else
	(mapping symbol-comparator))))))
