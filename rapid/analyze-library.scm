;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \section{Qualified identifiers}

;;> A qualified identifier is a triple consisting of a library name,
;;> an identifier naming a top-level binding of that library (before
;;> renaming), and a syntax object to locate syntax error messages.

;;; Library names

(define (library-element? obj)
   (or (symbol? obj)
       (and (exact-integer? obj) (>= obj 0))))

(define (library-element=? element1 element2)
  (equal? element1 element2))

(define (library-element<? element1 element2)
  (if (symbol? element1)
      (or (exact-integer? element2)
	  (symbol<? element1 element2))
      (and (exact-integer? element2)
	   (< element1 element2))))

(define (library-element-hash element)
  (if (symbol? element)
      (symbol-hash element)
      (number-hash element)))

(define (symbol<? symbol1 symbol2)
  (string<? (symbol->string symbol1) (symbol->string symbol2)))

;;> \procedure{(unwrap-library-name library-name-syntax)}

;;> Returns the Scheme datum representing the R7RS library name
;;> wrapped in the syntax object \var{library-name-syntax}.  If
;;> \var{library-name-syntax} does not wrap a proper R7RS library name
;;> as a Scheme datum, a syntax error object is signaled.  If the
;;> exception is continued, \scheme{unwrap-library-name} returns
;;> \scheme{#f}.

(define (unwrap-library-name library-name-syntax)
  (let ((datum (syntax->datum library-name-syntax)))
    (if (library-name? datum)
	datum
	(raise-syntax-error library-name-syntax "invalid library name"))))

(define (unwrap-identifier identifier-syntax)
  (let ((datum (syntax->datum identifier-syntax)))
    (if (symbol? datum)
	datum
	(raise-syntax-error identifier-syntax "invalid identifier"))))

(define (unwrap-identifiers identifiers)
  (fold (lambda (identifier-syntax identifier-mapping)
	  (or (and-let* ((identifier (unwrap-identifier identifier-syntax)))
		(receive (identifier-mapping obj)
		    (mapping-search identifier-mapping
				    identifier
				    (lambda (insert ignore)
				      (insert identifier-syntax #f))
				    (lambda (identifier old-identifier-syntax update remove)
				      (raise-syntax-error identifier-syntax
							  "identifier ‘~a’ already in list"
							  identifier)
				      (raise-syntax-note old-identifier-syntax
							 "first occurance was here")
				      (update identifier old-identifier-syntax #f)))
		  identifier-mapping))
	      identifier-mapping))
	(mapping symbol-comparator) identifiers))

;;;; Procedures

;;> \section{Procedures}

;;; Qualified identifiers

(define-record-type <qualified-identifier>
  (%make-qualified-identifier context library-name symbol)
  qualified-identifier?
  (context qualified-identifier-context)
  (library-name qualified-identifier-library-name)
  (symbol qualified-identifier-symbol))

;;> \procedure{(make-qualified-identifier context library-name symbol)}

;;> Returns a qualified identifier in the library \var{library-name}
;;> with name \var{symbol} and syntax object \var{context}.

(define (make-qualified-identifier context library-name symbol)
  (assume (or (eq? #f context) (syntax? context)))
  (assume (library-name? library-name))
  (assume (symbol? symbol))
  (%make-qualified-identifier context library-name symbol))

;;> \procedure{(qualified-identifier? object)}

;;> Returns \scheme{#t} if \var{object} is a qualified identifier, and
;;> \scheme{#f} otherwise.

;;> \procedure{(qualified-identifier-library-name
;;> qualified-identifier)}

;;> Returns the library name of the \var{qualified-identifier}.

;;> \procedure{(qualified-identifier-symbol qualified-identifier)}

;;> Returns the name of the \var{qualified-identifier}.

;;> \procedure{(qualified-identifier-context qualified-identifier)}

;;> Returns the syntax object of the \var{qualified-identifier}.

;;> \procedure{(analyze-library library-name declarations)}

;;> Analyzes the library with name \var{library-name} and declarations
;;> \var{declarations}.  The \var{library-name} is a Scheme datum
;;> representing an R7RS library name, and the \var{declarations} are
;;> a list of syntax objects each wrapping a Scheme datum representing
;;> an R7RS library declaration.  Returns four values: The first value
;;> is a mapping that maps all the identifiers exported by the library
;;> to their original bindings, which are identified by qualified
;;> identifiers.  The second value is a mapping that maps all the
;;> identifiers imported into the library to their original bindings,
;;> which are likewise identified by qualified identifiers.  The third
;;> value is a set containing the library names of those libraries
;;> upon which this library is dependent, directly or indirectly.  The
;;> fourth value is the library's body, a sequence of syntax objects
;;> each wrapping a top-level command or definition.

(define (analyze-library library-name declarations)
  (let*-values (((export-specs import-sets body)
		 (expand-library-declarations declarations))
		((imports)
		 (import-mapping import-sets))
		((exports)
		 (export-mapping library-name export-specs imports))
		((dependencies)
		 (dependency-set imports)))
    (values exports imports dependencies body)))

;;> \procedure{(expand-cond-expand clauses)}

;;> Expands the given clauses as if they were clauses of a cond-expand
;;> expression and returns a list of syntax objects.

(define (expand-cond-expand clauses)
  (let loop ((declarations #f) (clauses clauses))
    (if (null? clauses)
	(or declarations '())
	(let ((clause-syntax (car clauses)))
	  (match* (strip #f) clause-syntax
	    ((else ,declaration* ...)
	     (cond
	      ((null? (cdr clauses))
	       (loop (or declarations declaration*) (cdr clauses)))
	      (else
	       (raise-syntax-error clause-syntax "else clause not last")
	       (loop declarations (cdr clauses)))))
	    ((,feature-requirement ,declaration* ...)
	     (if (feature? feature-requirement)
		 (loop (or declarations declaration*) (cdr clauses))
		 (loop declarations (cdr clauses))))
	    (,clause
	     (raise-syntax-error clause "bad cond-expand clause")
	     (loop declarations (cdr clauses))))))))

(define (expand-library-declarations declarations)
  (let loop ((export-specs '()) (import-sets '()) (body '()) (declarations declarations))
    (if (null? declarations)
	(values (reverse export-specs)
		(reverse import-sets)
		(reverse body))
	(let ((loop (lambda (export-specs import-sets body new-declarations)
		      (loop export-specs import-sets body
			    (append-reverse new-declarations (cdr declarations)))))
	      (declaration (car declarations)))
	  (syntax-match declaration
	    ((export ,export-spec* ...)
	     (loop (append-reverse export-spec* export-specs)
		   import-sets
		   body
		   '()))
	    ((import ,import-set* ...)
	     (loop export-specs
		   (append-reverse import-set* import-sets)
		   body
		   '()))
	    ((begin ,form* ...)
	     (loop export-specs
		   import-sets
		   (append-reverse form* body)
		   '()))
	    ((include ,filename* ...)
	     (loop export-specs
		   import-sets
		   (read-files-reverse declaration filename* body #f)
		   '()))
	    ((include-ci ,filename* ...)
	     (loop export-specs
		   import-sets
		   (read-files-reverse declaration filename* body #t)
		   '()))
	    ((include-library-declarations ,filename* ...)
	     (loop export-specs
		   import-sets
		   body
		   (read-files-reverse declaration filename* '() #f)))
	    ((cond-expand ,clause* ...)
	     (loop export-specs
		   import-sets
		   body
		   (reverse (expand-cond-expand clause*))))
	     (,declaration
	      (raise-syntax-error declaration "invalid library declaration")
	      (loop export-specs import-sets body '())))))))

(define (feature? feature-requirement)
  (let feature? ((feature-requirement feature-requirement))
    (match* (strip #f) feature-requirement
      (,symbol
       (guard (symbol? (unwrap-syntax symbol)))
       (feature-identifier? (unwrap-syntax symbol)))
      ((library ,(unwrap-library-name -> library-name))
       (and library-name (library-importable? feature-requirement library-name)))
      ((and ,feature-requirement* ...)
       (fold (lambda (feature-requirement acc)
	       (and (feature? feature-requirement) acc))
	     #t feature-requirement*))
      ((or ,feature-requirement* ...)
       (fold (lambda (feature-requirement acc)
	       (or (feature? feature-requirement) acc))
	     #f feature-requirement*))
      ((not ,feature-requirement)
       (not (feature? feature-requirement)))
      (,_
       (raise-syntax-error feature-requirement "bad feature requirement")))))

(define (import-mapping import-sets)
  (fold (lambda (import-set imports)
	  (let ((import-mapping (make-import-mapping import-set)))
	    (mapping-fold
	     (lambda (target source imports)
	       (mapping-update
		imports
		target
		(lambda (old-source)
		  (unless (equal? old-source source)
		    (raise-syntax-error import-set
					"identifier ‘~a’ already imported with a different binding"
					target))
		  
		  old-source)
		(lambda () source)))
	     imports import-mapping)))
	(mapping symbol-comparator) import-sets))

(define (make-import-mapping import-set)
  (syntax-match import-set
    ((,keyword (,import-set* ...) ,identifier* ...)
     (syntax-match import-set
       ((only ,(make-import-mapping -> import-set) ,identifier* ...)
	(let ((identifiers (unwrap-identifiers identifier*)))
	  (mapping-fold (lambda (identifier identifier-syntax imports)
			  (or (and-let*
				  ((source
				    (or (mapping-ref/default import-set identifier #f)
					(raise-syntax-error
					 identifier-syntax
					 "identifier ‘~a’ not found in original set"
					 identifier))))
				(mapping-set imports identifier source))
			      imports))
			(mapping symbol-comparator) identifiers)))
       ((except ,(make-import-mapping -> import-set) ,identifier* ...)
	(let ((identifiers (unwrap-identifiers identifier*)))
	  (mapping-fold (lambda (identifier identifier-syntax imports)
			  (receive (imports obj)
			      (mapping-search imports
					      identifier
					      (lambda (insert ignore)
						(raise-syntax-error
						 identifier-syntax
						 "identifier ‘~a’ not found in original set"
						 identifier)
						(ignore #f))
					      (lambda (old-identifier old-source update remove)
						(remove #f)))
			    imports))
			import-set identifiers)))
       ((prefix ,(make-import-mapping -> import-set) ,identifier-syntax)
	(or (and-let* ((identifier (unwrap-identifier identifier-syntax)))
	      (mapping-map (lambda (target source)
			     (values (symbol-append identifier target) source))
			   symbol-comparator import-set))
	    import-set))
       ((rename ,(make-import-mapping -> import-set) (,identifier1* ,identifier2*) ...)
	(receive (new-identifier* new-syntax* qid* import-set)
	    (let loop ((identifier1* identifier1*)
		       (identifier2* identifier2*)
		       (old-identifiers (mapping symbol-comparator))
		       (import-set import-set))
	      (let ((continue (lambda (old-identifiers import-set)
				(loop (cdr identifier1*) (cdr identifier2*)
				      old-identifiers import-set))))
		(if (null? identifier1*)
		    (values '() '() '() import-set)
		    (let* ((old-syntax (car identifier1*))
			   (new-syntax (car identifier2*))
			   (old (unwrap-identifier old-syntax))
			   (new (unwrap-identifier new-syntax)))
		      (if (and old new)
			  (receive (old-identifiers ok)
			      (mapping-search old-identifiers
					      old
					      (lambda (insert ignore)
						(insert old-syntax #t))
					      (lambda (previous previous-syntax update remove)
						(raise-syntax-error
						 old-syntax
						 "identifier ‘~a’ already in rename list"
						 old)
						(raise-syntax-note previous-syntax
								   "first occurance was here")
						(update previous previous-syntax #f)))
			    (if ok
				(receive (import-set qid)
				    (mapping-search import-set
						    old
						    (lambda (insert ignore)
						      (raise-syntax-error
						       old-syntax
						       "identifier ‘~a’ not found in original set"
						       old)
						      (ignore #f))
						    (lambda (old old-qid update remove)
						      (remove old-qid)))
				  (if qid
				      (receive (new-identifier* new-syntax* qid* import-set)
					  (continue old-identifiers import-set)
					(values (cons new new-syntax*)
						(cons new-syntax new-syntax*)
						(cons qid qid*)
						import-set))    
				      (continue old-identifiers import-set)))
				(continue old-identifiers import-set)))))
		    (continue old-identifiers import-set))))
	  (fold (lambda (new new-syntax qid import-set)
		  (mapping-update import-set
				  new
				  (lambda (previous-qid)
				    (unless (equal? previous-qid qid)
				      (raise-syntax-error
				       new-syntax
				       "identifier ‘~a’ already imported with a different binding"
				       new))
				    previous-qid)
				  (lambda ()
				    qid)))
		import-set new-identifier* new-syntax* qid*)))))
    (,(unwrap-library-name -> library-name)
     (if library-name
	 (library-exports import-set library-name)
	 (mapping symbol-comparator)))))

(define (export-mapping library-name export-specs imports)
  (fold (lambda (export-spec exports)
	  (syntax-match export-spec
	    ((rename ,identifier1 ,identifier2)
	     (exports-add library-name exports identifier1 identifier2 imports))
	    (,identifier
	     (exports-add library-name exports identifier identifier imports))))
	(mapping symbol-comparator) export-specs))

(define (exports-add library-name exports source-syntax target-syntax imports)
  (or (and-let*
	  ((source (unwrap-identifier source-syntax))
	   (target (unwrap-identifier target-syntax)))
	(receive (exports obj)
	    (mapping-search
	     exports
	     target
	     (lambda (insert ignore)
	       (insert
		(mapping-ref/default imports
				     source
				     (make-qualified-identifier target-syntax
								library-name
								source))
		#f))
	     (lambda (target source update remove)
	       (raise-syntax-error target-syntax
				   "identifier ‘~a’ already exported"
				   target)
	       (update target source #f)))
	  exports))
      exports))

(define (dependency-set imports)
  (let*
      ((library-names
	(mapping-fold
	 (lambda (identifier qid library-names)
	   (set-adjoin library-names
		       (qualified-identifier-library-name qid)))
	 (set library-name-comparator) imports)))
    (set-fold
     (lambda (library-name dependencies)
       (set-union dependencies 
		  (library-dependencies library-name)))
     library-names library-names)))

;;; Utility procedures

(define (symbol-append symbol1 symbol2)
  (string->symbol (string-append (symbol->string symbol1) (symbol->string symbol2))))

(define (strip syntax)
  (let ((datum (unwrap-syntax syntax)))
    (if (identifier? datum)
	(identifier->symbol datum)
	datum)))

;;; Access to parameters

;;> \procedure{(feature-identifier? symbol)}

;;> Returns \scheme{#t} if \var{symbol} is a feature identifier in the
;;> current dynamic environment, and \scheme{#f} otherwise.

(define (feature-identifier? symbol)
  ((feature-identifier-predicate) symbol))

;;> \procedure{(library-importable? context library-name)}

;;> Returns \scheme{#t} if, in the current dynamic environment, the
;;> library with name \var{library-name} can be imported, and
;;> \scheme{#f} otherwise.  \var{Context} is either \scheme{#f} or a
;;> syntax object that can be referred to if \scheme{library-exports}
;;> raises a syntax error object.

(define (library-importable? context library-name)
  ((library-importability-predicate) context library-name))

;;> \procedure{(library-exports context library-name)}

;;> Returns a mapping that maps all the identifiers exported by the
;;> library with name \var{library-name} to their original bindings,
;;> which are identified by qualified identifiers, or \scheme{#f} if
;;> there is no such library known.  \var{Context} is either
;;> \scheme{#f} or a syntax object that can be referred to if
;;> \scheme{library-exports} raises a syntax error object.

(define (library-exports context library-name)
  ((library-exports-accessor) context library-name))

;;> \procedure{(library-dependencies library-name)}

;;> Returns a set containing the library names of those libraries upon
;;> which, directly or indirectly the library with name
;;> \var{library-name} depends.  It is an error to invoke
;;> \scheme{library-dependencies} on a library name before invoking
;;> \scheme{library-exports} upon that name.

(define (library-dependencies library-name)
  ((library-dependencies-accessor) library-name))

;;;; Parameter objects

;;> \section{Parameter objects}

;;> \procedure{(feature-identifier-predicate)}

;;> Returns the current predicate queried when invoking
;;> \scheme{feature-identifier?}.  This procedure is a parameter
;;> object.  The initial binding is not specified.

(define feature-identifier-predicate
  (make-parameter (lambda (symbol)
		    (assume #f))))

;;> \procedure{(library-importability-predicate)}

;;> Returns the current predicate queried when invoking
;;> \scheme{library-importable?}.  This procedure is a parameter
;;> object.  The initial binding is not specified.

(define library-importability-predicate
  (make-parameter (lambda (context library-name)
		    (assume #f))))

;;> \procedure{(library-exports-accessor)}

;;> Returns the current accessor queried when invoking
;;> \scheme{library-exports}.  This procedure is a parameter
;;> object.  The initial binding is not specified.

(define library-exports-accessor
  (make-parameter (lambda (context library-name)
		    (assume #f))))

;;> \procedure{(library-dependencies-accessor)}

;;> Returns the current accessor queried when invoking
;;> \scheme{library-dependencies}.  This procedure is a parameter
;;> object.  The initial binding is not specified.

(define library-dependencies-accessor
  (make-parameter (lambda (library-name)
		    (assume #f "library-dependencies-accessor: has to be bound"))))

;;;; Comparators

;;> \section{Comparators}

;;> \procedure{symbol-comparator}

;;> Comparator for symbols.  Has an ordering predicate and a hash
;;> function.

(define symbol-comparator (make-comparator symbol? eq? symbol<? symbol-hash))

(define library-element-comparator
  (make-comparator library-element? library-element=? library-element<? library-element-hash))

;;> \procedure{library-name-comparator}

;;> Comparator for R7RS library names as Scheme datums.  Has an
;;> ordering predicate and a hash function.

(define library-name-comparator
  (make-list-comparator library-element-comparator list? null? car cdr))

;;> \procedure{(library-name? object)}

;;> Type-test predicate for library names.  Returns \scheme{#t} if
;;> \var{object} is a Scheme datum representing a library name, and
;;> \scheme{#f} otherwise.

(define library-name? (comparator-type-test-predicate library-name-comparator))
