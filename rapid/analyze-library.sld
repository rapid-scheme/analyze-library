;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Analyze R7RS library definitions.

(define-library (rapid analyze-library)
  (export make-qualified-identifier
	  qualified-identifier?
	  qualified-identifier-context
	  qualified-identifier-library-name
	  qualified-identifier-symbol

	  analyze-library
	  expand-cond-expand
	  
	  feature-identifier?
	  library-importable?
	  library-exports
	  library-dependencies
	  library-name-comparator
	  feature-identifier-predicate
	  library-importability-predicate
	  library-exports-accessor
	  library-dependencies-accessor
	  symbol-comparator
	  library-name-comparator
	  library-name?
	  unwrap-library-name)
  (import (scheme base)
	  (rapid assume)
	  (rapid and-let)
	  (rapid receive)
	  (rapid comparator)
	  (rapid set)
	  (rapid mapping)
	  (rapid match)
	  (rapid syntax)
	  (rapid read)
	  (rapid list)
	  (rapid syntactic-environment))
  (include "analyze-library.scm"))
